function getAppPath(isAndroid) {
  if (process.env.BITRISE) {
    return process.env.BITRISE_APK_PATH;
  } else if (isAndroid) {
    return `${process.cwd()}/android/app/build/outputs/apk/debug/app-debug.apk`;
  } else {
    return `${process.cwd()}/ios/build/bitrisecitest/Build/Products/Debug-iphonesimulator/bitrisecitest.app`;
  }
}

module.exports.helpers = {
  getAppPath,
};
