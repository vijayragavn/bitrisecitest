const getAppPath = require('./helpers').helpers.getAppPath;
const commonConfig = require('./common.conf');

const waitforTimeout = 30 * 60000;
const commandTimeout = 30 * 60000;
const capabilities = {
  android: {
    waitforTimeout,
    commandTimeout,
    browserName: 'Android',
    platformName: 'Android',
    unicodeKeyboard: true,
    newCommandTimeout: 30 * 60000,
    resetKeyboard: true,
    noReset: true,
    platformVersion: '10.0',
    automationName: 'UiAutomator2',
    deviceName: 'Pixel_XL_API_29',
    app: getAppPath(true),
  },
  ios: {
    waitforTimeout,
    commandTimeout,
    browserName: 'iOS',
    platformName: 'iOS',
    unicodeKeyboard: true,
    newCommandTimeout: 30 * 60000,
    resetKeyboard: true,
    noReset: true,
    nativeInstrumentsLib: true,
    isolateSimDevice: true,
    platformVersion: '13.1',
    deviceName: 'iPhone 11',
    app: getAppPath(),
  },
};
const getCapability = () => {
  if (process.env.PLATFORM === 'android') {
    return [capabilities.android];
  } else if (process.env.PLATFORM === 'iOS') {
    console.info('Platform is iOS');
    return [capabilities.ios];
  }
  return [capabilities.ios, capabilities.android];
};
module.exports.config = Object.assign(commonConfig.config, {
  capabilities: getCapability(),
  services: ['appium'],
  maxInstances: 2,
  appium: {
    waitStartTime: 6000,
    args: {
      waitforTimeout,
      port: 4723,
      logFileName: 'appium.log',
      commandTimeout: 30000,
      address: '127.0.0.1',
      sessionOverride: true,
      noReset: true,
      autoAcceptAlerts: true,
      debugLogSpacing: false,
      appiumVersion: '1.15.1', // Appium module version
      autoGrantPermissions: true,
    },
  },
  port: 4723,
  baseUrl: 'http://127.0.0.1',
});
